import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        int num = 34;
        while (true) {
            System.out.println("Ededi daxil edin");
            Scanner sc = new Scanner(System.in);
            int a = sc.nextInt();
            if (a == 34) {
                System.out.println("Tebrikler tapdiniz!");
                break;
            } else if (a < 34) {
                System.out.println("Yuxari");
            } else if (a > 34) {
                System.out.println("Asagi");
            }
        }
    }
}
