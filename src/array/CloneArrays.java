package array;

public class CloneArrays {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,6};
        int[] arr2 = {1,2,3,4,5};
        boolean duplicate = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != arr2[i]) {
                duplicate = false;
                break;
            }
        }
        if (duplicate){
            System.out.println("Duplicate arrays");
        }else System.out.println("Not duplicate");
     }
}
