package array;

public class CountNumber {
    public static void main(String[] args) {
        int[] arr = {7, 34, 35, 12, 70};
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 7 == 0) {
                count++;
            }
        }
        System.out.println(count);
    }
}
