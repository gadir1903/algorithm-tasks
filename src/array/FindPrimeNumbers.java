package array;

public class FindPrimeNumbers {
    public static void main(String[] args) {
        int[] arr = {11,4, 35, 31, 3, 59, 22};
        for (int i = 0; i < arr.length; i++) {
            boolean primeNum = true;
            for (int j = 2; j < arr[i]; j++) {
                if (arr[i] % j == 0) {
                    primeNum = false;
                    break;
                }
            }
            if (primeNum) {
                System.out.println(arr[i]);
            }
        }
    }
}