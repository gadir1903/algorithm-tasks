package array;

public class FindSmallestEvenBiggestOdd {
    public static void main(String[] args) {
        int[] arr = {4, 78, 42, 27, 35, 55, 57};
        int smallestEven = arr[0];
        int biggestOdd = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < smallestEven && arr[i] % 2 == 0){
                smallestEven = arr[i];
            }
            if ((arr[i] % 3 == 0 && arr[i] % 2 != 0) || arr[i] % 5 == 0) {
                if (arr[i] > biggestOdd)
                    biggestOdd = arr[i];
            }
        }
        System.out.println(smallestEven);
        System.out.println(biggestOdd);
    }
}
