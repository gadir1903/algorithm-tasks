package array;

public class PalindromeElements {
    public static void main(String[] args) {

        int[] arr = {1, 2, 3, 4, 3, 2, 1};
        boolean palindrome = true;
        for (int first = 0, last = arr.length - 1; first < last; first++, last--) {
            if (arr[first] != arr[last]) {
                palindrome = false;
                break;
            }
        }
        if (palindrome) {
            System.out.println("Palindrome elements");
        } else System.out.println("not Palindrome");
    }
}
